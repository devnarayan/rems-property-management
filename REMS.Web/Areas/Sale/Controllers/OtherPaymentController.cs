﻿using REMS.Data;
using REMS.Data.Access;
using REMS.Data.DataModel;
using REMS.Web.App_Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace REMS.Web.Areas.Sale.Controllers
{
    public class OtherPaymentController : BaseController
    {
        //
        // GET: /Sale/OtherPayment/
        public ActionResult Index()
        {
            return View();
        }
        public string EditSearchPayment(string FlatId)
        {
            try
            {
                //string proName = PropertyID;

                //int pid = 0, ptype = 0, psize = 0;
                //if (proName == "? undefined:undefined ?" || proName == "All") proName = "All"; else pid = Convert.ToInt32(proName);
                REMSDBEntities context = new REMSDBEntities();
                // Search by name.
                List<OtherPaymentModel> model = new List<OtherPaymentModel>();

                //if (paymentfor == "All")
                //{
                    int proid = Convert.ToInt32(FlatId);
                    int sid = (from s in context.SaleFlats join f in context.Flats on s.FlatID equals f.FlatID where f.FlatID == proid select s.SaleID).FirstOrDefault();
                    var model1 = (from sale in
                                      context.SaleFlats
                                  join pay in context.PaymentOthers on sale.SaleID equals pay.SaleID
                                  where pay.SaleID==sid
                                  select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentOtherID, PaymentFor = pay.PaymentFor, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                    foreach (var v in model1)
                    {
                        string bdate = "";
                        if (v.PaymentDate != null)
                            bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                        // Set Color
                        model.Add(new OtherPaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentOtherID = v.PaymentID, PaymentFor = v.PaymentFor, PaymentNo = v.PaymentNo, PaymentStatus = v.PaymentStatus });
                    }
                //}
                //else
                //{
                //    var model1 = (from sale in
                //                      context.SaleFlats
                //                  join pay in context.PaymentOthers on sale.SaleID equals pay.SaleID
                //                  where pay.FlatName.Contains(searchtext) && pay.PaymentFor.Contains(paymentfor)
                //                  select new { TransactionID = pay.TransactionID, FlatName = pay.FlatName, CustomerName = pay.CustomerName, PaymentDate = pay.PaymentDate, PaymentMode = pay.PaymentMode, Remarks = pay.Remarks, Saleid = pay.SaleID, Amount = pay.Amount, PaymentID = pay.PaymentOtherID, PaymentFor = pay.PaymentFor, PaymentNo = pay.PaymentNo, PaymentStatus = pay.PaymentStatus }).AsEnumerable();
                //    foreach (var v in model1)
                //    {
                //        string bdate = "";
                //        if (v.PaymentDate != null)
                //            bdate = Convert.ToDateTime(v.PaymentDate).ToString("dd/MM/yyyy");
                //        // Set Color
                //        model.Add(new OtherPaymentModel { PaymentDateSt = bdate, TransactionID = v.TransactionID, FlatName = v.FlatName, CustomerName = v.CustomerName, PaymentDate = v.PaymentDate, PaymentMode = v.PaymentMode, Remarks = v.Remarks, SaleID = v.Saleid, Amount = v.Amount, PaymentOtherID = v.PaymentID, PaymentFor = v.PaymentFor, PaymentNo = v.PaymentNo, PaymentStatus = v.PaymentStatus });
                //    }
                //}

                return Newtonsoft.Json.JsonConvert.SerializeObject(model);
            }
            catch (Exception ex)
            {
                REMSDBEntities context = new REMSDBEntities();

                Failure = "Invalid search query, please try again.";
                Helper h = new Helper();
                h.LogException(ex);
                return Newtonsoft.Json.JsonConvert.SerializeObject("");
            }
        }
	}
}